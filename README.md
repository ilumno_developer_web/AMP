### Ilumno AMP
![](https://www.ilumno.com/assets/icons/apple-icon-72x72.png)

1.	Crear una página básica AMP
2.	Incluir una imagen en la página AMP
3.	Insertar un video de YouTube en la página AMP


# Visitar
[Ver](https://ilumno_developer_web.gitlab.io/AMP/ "Ver")

![](https://www.adslzone.net/app/uploads-adslzone.net/2022/01/webs-moviles-mas-rapidas-amp-google.jpg)

